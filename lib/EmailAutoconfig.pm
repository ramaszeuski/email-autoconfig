package EmailAutoconfig;
use Mojo::Base 'Mojolicious', -signatures;

# This method will run once at server start
sub startup ($self) {

  # Load configuration from config file
  my $config = $self->plugin('NotYAMLConfig');

  # Konfigurace z ENV ma prednost
  KEY:
  foreach my $key ( keys %ENV ) {
      if ( $key =~ /^CFG_(.+)/i ) {
          $config->{lc($1)} = $ENV{$key};
      }
  }
  $self->helper( cfg => sub { return $config; } );

  # Configure the application
  $self->secrets($config->{secrets});

  # Router
  my $r = $self->routes;

#  $r->get('/autodiscover/autodiscover.json')->to('Autodiscover#json');
  $r->get('/autodiscover/autodiscover.json')->to('Autodiscover#activesync');
  $r->post('/autodiscover/autodiscover.xml')->to('Autodiscover#main');
  $r->any('/activesync')->to('Autodiscover#activesync');
  $r->get('/autodiscover/autodiscover.xml')->to(cb => sub { shift->render('autodiscover', format => 'xml', handler => 'ep'); });
  $r->get('/mail/config-v1.1.xml')->to(cb => sub { shift->render('autoconfig', format => 'xml', handler => 'ep'); });

}

1;
