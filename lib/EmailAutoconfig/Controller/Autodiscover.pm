package EmailAutoconfig::Controller::Autodiscover;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub main ($c) {
    $c->app->log->debug($c->req->to_string);
    $c->render('autodiscover', format => 'xml', handler => 'ep');
}

sub json ($c) {

    my $protocol = $c->param('Protocol');
    my $url = '/autodiscover/autodiscover.xml';

    if ($protocol eq 'ActiveSync') {
        $url = '/activesync';
    }

    $c->render(json => {
        Protocol => $protocol,
        Url      => $c->cfg->{base_url} . $url,
    });
}

sub activesync ($c) {
    $c->app->log->debug($c->req->to_string);
    $c->render(json => {
      info => {
        name    => $c->config->{provider_name},
        url     => $c->config->{provider_url},
#        domain  => ''
      },
      server => {
        imap => {
          host => $c->config->{imap_host},
          port => $c->config->{imap_port},
          socket => "SSL"
        },
        smtp => {
          host => $c->config->{smtp_host},
          port => $c->config->{smtp_port},
          socket => "SSL"
        },
        domain_required => \1
      },
      ttl => 168
    });
}

1;
