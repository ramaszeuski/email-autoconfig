FROM debian:bullseye-slim

#RUN apt-get update && apt-get install -y \
#    wget make gcc libc-dev \
#    cpanminus \
#    build-essential \
#    libio-socket-ssl-perl \
#    libmodule-build-perl \
#    libnet-ssleay-perl \
#    libjson-perl \
#    libyaml-dev

RUN apt-get update && apt-get install -y \
    libmojolicious-perl \
    libjson-perl

#RUN cpanm \
#    Mojolicious \

ADD . /opt/app
WORKDIR /opt/app

USER nobody
EXPOSE 3000
CMD /opt/app/script/email_autoconfig daemon
